
/**
 * Created by Ebubekir.
 */
public class StoogeSort extends AlgorithmComplexity {

    public void stoogeSort(int[] array) {
        stoogeSort(array, 0, array.length - 1);
    }

    public void stoogeSort(int[] array, int i, int j) {
        int tmp;
        if (array[j] < array[i]) {
            tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
        if (j - i > 1) {
            int t = (j - i + 1) / 3;
            stoogeSort(array, i, j - t);
            stoogeSort(array, i + t, j);
            stoogeSort(array, i, j - t);
        }
    }

}
