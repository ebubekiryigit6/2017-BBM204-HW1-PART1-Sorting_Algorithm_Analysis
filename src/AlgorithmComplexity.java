import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ebubekir.
 */


/******************************************************************************************************************************
 *      Algorithms are invoked without dividing into individual functions so that the algorithm analysis can be seen better.  *
 *****************************************************************************************************************************/
public class AlgorithmComplexity {

    /******************************************************************************************************
     *   To check the algorithms, you can remove the comments in front of the "printArray()" functions.   *
     ******************************************************************************************************/


    /**
     * Executes 5 algorithms "exp" times. And prints their average execution time.
     * @param N_ARRAY  array of N values
     * @param numOfTry  runs algorithms "numOfTry" times
     */
    protected void printAlgorithmsAverageTimes(int[] N_ARRAY, double numOfTry){

        int N;   // number of elements

        for (int value : N_ARRAY) {

            N = value;

            double RadixSum = 0.0;
            double ShakerSum = 0.0;
            double StoogeSum = 0.0;
            double MaxSubSum = 0.0;
            double SecondLargestSum = 0.0;

            for (int i = 0; i < numOfTry; i++) {   // Each algorithm tries as much as "exp" and takes an average value.

                int[] radixArray = getRandomIntegerArray(N);
                RadixSort radixSort = new RadixSort();
                YGTStopwatch radixTimer = new YGTStopwatch();
                radixSort.radixSort(radixArray);
                double radixSortTime = radixTimer.elapsedTime();
                RadixSum += radixSortTime;

                //System.out.printf("Radix Sort    execution time is %f with  %d elements\n", radixSortTime, N);
                //printArray(radixArray);

                int[] shakerArray = getRandomIntegerWithNegatives(N);
                ShakerSort shakerSort = new ShakerSort();
                YGTStopwatch shakerTimer = new YGTStopwatch();
                shakerSort.shakerSort(shakerArray);
                double shakerSortTime = shakerTimer.elapsedTime();
                ShakerSum += shakerSortTime;

                //System.out.printf("Shaker Sort   execution time is %f with  %d elements\n",shakerSortTime,N);
                //printArray(shakerArray);

                int[] stoogeArray = getRandomIntegerWithNegatives(N);
                StoogeSort stoogeSort = new StoogeSort();
                YGTStopwatch stoogeTimer = new YGTStopwatch();
                stoogeSort.stoogeSort(stoogeArray,0,N-1);
                double stoogeSortTime = stoogeTimer.elapsedTime();
                StoogeSum += stoogeSortTime;

                //System.out.printf("Stooge Sort   execution time is %f with  %d elements\n",stoogeSortTime,N);
                //printArray(stoogeArray);


                int[] maxSubArray = getRandomIntegerWithNegatives(N);
                MaxSubArray maxObject = new MaxSubArray();
                YGTStopwatch maxSubTimer = new YGTStopwatch();
                int MAX = maxObject.maxSubArray(maxSubArray);
                double maxSubTime = maxSubTimer.elapsedTime();
                MaxSubSum += maxSubTime;

                //System.out.printf("Max Sub Array execution time is %f with  %d elements and MAX is: %d\n",maxSubTime,N,MAX);
                //printArray(maxSubArray);


                ArrayList<Integer> secondLargestArray = getRandomArrayListWithNegatives(N);
                SecondLargestElement secondLargestElement = new SecondLargestElement();
                YGTStopwatch secondLargestTimer = new YGTStopwatch();
                int element = secondLargestElement.getSecondLargestElement(secondLargestArray, 0, N - 1);
                double secondLargestTime = secondLargestTimer.elapsedTime();
                SecondLargestSum += secondLargestTime;

                //System.out.printf("2nd Element   execution time is %f with  %d elements and 2nd Element is: %d\n",secondLargestTime,N,element);
                //Collections.sort(secondLargestArray);
                //printArrayList(secondLargestArray);
            }

            //}

            System.out.printf("\n N = %d\nAverage Radix = %f\n" +
                    "Average Shaker = %f\n" +
                    "Average Stooge = %f\n" +
                    "Average MaxSub = %f\n" +
                    "Average SecondLargest = %f\n\n", N, RadixSum / numOfTry, ShakerSum / numOfTry, StoogeSum / numOfTry, MaxSubSum / numOfTry, SecondLargestSum / numOfTry);

        }
    }


    protected void printAlgorithmTimesWithN(int N){

        int[] radixArray = getRandomIntegerArray(N);
        RadixSort radixSort = new RadixSort();
        YGTStopwatch radixTimer = new YGTStopwatch();
        radixSort.radixSort(radixArray);
        double radixSortTime = radixTimer.elapsedTime();

        System.out.printf("Radix Sort    execution time is %f with  %d elements\n", radixSortTime, N);
        //printArray(radixArray);

        int[] shakerArray = getRandomIntegerWithNegatives(N);
        ShakerSort shakerSort = new ShakerSort();
        YGTStopwatch shakerTimer = new YGTStopwatch();
        shakerSort.shakerSort(shakerArray);
        double shakerSortTime = shakerTimer.elapsedTime();

        System.out.printf("Shaker Sort   execution time is %f with  %d elements\n",shakerSortTime,N);
        //printArray(shakerArray);

        int[] stoogeArray = getRandomIntegerWithNegatives(N);
        StoogeSort stoogeSort = new StoogeSort();
        YGTStopwatch stoogeTimer = new YGTStopwatch();
        stoogeSort.stoogeSort(stoogeArray);
        double stoogeSortTime = stoogeTimer.elapsedTime();

        System.out.printf("Stooge Sort   execution time is %f with  %d elements\n",stoogeSortTime,N);
        //printArray(stoogeArray);


        int[] maxSubArray = getRandomIntegerWithNegatives(N);
        MaxSubArray maxObject = new MaxSubArray();
        YGTStopwatch maxSubTimer = new YGTStopwatch();
        int MAX = maxObject.maxSubArray(maxSubArray);
        double maxSubTime = maxSubTimer.elapsedTime();

        System.out.printf("Max Sub Array execution time is %f with  %d elements and MAX is: %d\n",maxSubTime,N,MAX);
        //printArray(maxSubArray);

        ArrayList<Integer> secondLargestArray = getRandomArrayListWithNegatives(N);
        SecondLargestElement secondLargestElement = new SecondLargestElement();
        YGTStopwatch secondLargestTimer = new YGTStopwatch();
        int element = secondLargestElement.getSecondLargestElement(secondLargestArray, 0, N - 1);
        double secondLargestTime = secondLargestTimer.elapsedTime();

        System.out.printf("2nd Element   execution time is %f with  %d elements and 2nd Element is: %d\n",secondLargestTime,N,element);
        //Collections.sort(secondLargestArray);   // sort array to see second largest
        //printArrayList(secondLargestArray);
    }

    /**
     * Creates an array of random only positive integers.
     * @param N how many elements
     * @return created array
     */
    public int[] getRandomIntegerArray(int N){
        int[] array = new int[N];
        for (int i = 0; i < N; i++){
            int temp = new Random().nextInt(Integer.MAX_VALUE);
            array[i] = temp;
        }
        return array;
    }


    /**
     * Creates an array of random integers.
     * @param N how many elements
     * @return created array
     */
    public int[] getRandomIntegerWithNegatives(int N){
        int[] array = new int[N];
        for (int i = 0; i < N; i++){
            int temp = new Random().nextInt(100) - 50;
            array[i] = temp;
        }
        return array;
    }

    /**
     * Creates an arraylist of random integers.
     * @param N how many elements
     * @return created array
     */
    public ArrayList<Integer> getRandomArrayListWithNegatives(int N){
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < N; i++) {
            int temp = new Random().nextInt(100) - 50;
            arrayList.add(temp);
        }
        return arrayList;

    }

    /**
     * prints elements of given array
     * @param array
     */
    public void printArray(int[] array){
        for (int element : array) {
            System.out.printf("%d ", element);
        }
        System.out.println();
    }

    /**
     * prints elements of given array
     * @param arrayList
     */
    public void printArrayList(ArrayList<Integer> arrayList){
        for (int element : arrayList) {
            System.out.printf("%d ", element);
        }
        System.out.println();
    }

}
