import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ebubekir.
 */



/**
 *      This algorithm finds second largest element with tournament based algorithm.
 *
 *         |
          / \
         |   |         This algorithm compares elements like this.
        / \ / \
        x x x x
 *
 *
 */
public class SecondLargestElement extends AlgorithmComplexity {

    private HashMap<Integer,ArrayList<Integer>> defeatedMap;

    public SecondLargestElement(){
        defeatedMap = new HashMap<Integer, ArrayList<Integer>>();
    }

    /*
        Adds defeated elements in winners list
     */
    private void fillMap(int key, int value){
        if (defeatedMap.containsKey(key)){
            defeatedMap.get(key).add(value);
        }
        else {
            ArrayList<Integer> temp = new ArrayList<Integer>();
            temp.add(value);
            defeatedMap.put(key,temp);
        }
    }

    /*
        Finds largest element as described above
     */
    private int largestElement(ArrayList<Integer> array, int min, int max) {
        if (max < min) {
            return Integer.MIN_VALUE;
        } else if (min == max) {
            return array.get(min);
        } else if (max - min == 1) {
            fillMap(array.get(min),array.get(max));
            if (array.get(min)> array.get(max)) {
                fillMap(array.get(min),array.get(max));
                return array.get(min);
            } else {
                fillMap(array.get(max),array.get(min));
                return array.get(max);
            }
        } else {
            int tempMin = min + ((max-min) / 2);

            int x = largestElement(array,min,tempMin);
            int y = largestElement(array,tempMin+1,max);

            if (x > y){
                fillMap(x,y);
                return x;
            }
            else {
                fillMap(y,x);
                return y;
            }
        }
    }

    public int getSecondLargestElement(ArrayList<Integer> array, int min, int max){
        int largest = largestElement(array,min,max);
        ArrayList<Integer> temp = defeatedMap.get(largest);
        return largestElement(temp,0,temp.size()-1);
    }



}
