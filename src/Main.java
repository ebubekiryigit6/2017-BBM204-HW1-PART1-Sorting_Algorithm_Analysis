public class Main {

    public static void main(String[] args) {

        int[] N_ARRAY = {100,300,500,700,1100,1300,1500,1700,1900,2100,2300,2500};   // N values
        double exp = 10.0;     // number of trials

        /**
         *  This function calculates execution times of 5 algorithms.
         *  And prints elapsed time.
         */
        AlgorithmComplexity algorithmComplexity = new AlgorithmComplexity();
        algorithmComplexity.printAlgorithmTimesWithN(2500);    // prints all of 5 algorithims execution times


        System.out.printf("\n\nAfter 3 seconds, \n" +
                "The program will run each algorithm %.0f times\n" +
                "according to the value of N and \n" +
                "Program will calculate the average times.\n",exp);
        System.out.println("The duration of this process may vary depending on the value of N and the performance of the computer.\n");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {

        }

        /**
         *  This function runs all given algorithms up to "exp" and calculates the average time.
         *  And prints all of 5 algorithms average times
         */
        algorithmComplexity.printAlgorithmsAverageTimes(N_ARRAY,exp);

    }


}

